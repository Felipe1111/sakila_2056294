<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*primera ruta laravel
Route::get("Prueba", function(){
	echo "Hola, vaya parecese que todo anda funcionando bien";
});*/

//control
	Route::get("categorias", "CategoriaController@index");

//ruta que muestre el el formulario
	Route::get("categorias/create", "CategoriaController@create");

//guardra la nueva categoria en BD
	Route::post("categorias/store", "CategoriaController@store");

//rutas de actualizar
	Route::get("categorias/edit/{category_id}", "CategoriaController@edit");
	Route::post("categorias/update/{category_id}", "CategoriaController@update");
	