<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //acccion:metodo del controlador
    public function index(){
    	//seleccionar las categorias existentes
    	$categorias = Categoria::paginate(5);
        return view("categorias.index")->with("categorias", $categorias);
    }

    //mostrar el formulario de añadir categoria
    public function create(){
        return view("categorias.new");
    }

    //llegan datos desde el formulario de create
    //guarda la categoria en la BD
    public function store(Request $r){
        //$_POST= arrglo de php
        //almacena informacion que llega desde formularios
        //var_dump($_POST);
        //validacion

        //1 establecer reglas de validacion para cada campo
        $reglas = [
            "categoria" => ["required", "alpha"]
        ];
        $mensaje=[
            "required" => "Campo requrido",
            "alpha" => "Solo letras"
        ];
        //2 objeto validador
        $validador = Validator::make($r->all(), $reglas, $mensaje);
        $categoria = new Categoria();

        //3 validar : metodo fails returna true si la validacion falla
        if($validador->fails()){
            //codigo falla
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codigo correcto
        }
        
        //asignar el nombre traer datos del formulario categoria
            $categoria->name = $r->input("categoria");
        //guardar categoria
            $categoria->save();
        //letrero exito
            echo "Categoria guardada";
        //redireccecion con detos de sesion
            return redirect('categorias/create')->with("mensaje", "Categoria Guardada");
    }

    public function edit($category_id){
        //seleccionar categoria a editr
        $categoria = Categoria::find($category_id);
        //mostrar la vista de actualizacion
        //levar datos d categoria
        return view("categorias.edit")->with("categoria", $categoria);
    }

    public function update($category_id){
        $categoria = Categoria::find($category_id);
        //editarla
        $categoria->name = $_POST['categoria'];
        //guardar cambios
        $categoria->save();
        return redirect("categorias/edit/$category_id")->with("mensaje", "Categoria editada");
    }
}
